# Ready DemoApp

Generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

The project is a Webapp that demonstrates integration with Ready JS library and can be configured with Ready Mobile App as an Authenticated Tile 
and communicate with Institution Servers and Ready Public APIs. The Webapp is configured to work with TestSchools only.

## Webapp Flow

### Select the Environment

The environment determines the Institution Servers environment (US, Can and Us-Staging) that will be called from the Webapp.

### View the environment

Based on the environment selected, you will be directed to a new page which displays

1. User Token: fetched from cookies using Ready JS library. [Reference](https://gitlab.com/ready-education-public/integration-js/-/blob/master/README.md)
2. Access Token: to access the Ready Public API. It's generated from CampusCloud and is specific to the school (TestSchool in this scenario). [Reference](https://support.readyeducation.com/hc/en-us/articles/360035456133-Ready-Application-Programming-Interface-API-Overview)
3. Backend Service URL: aka Institution Servers. For Demo Webapp it's configured to call Ready ODIN API
4. Click `Submit` button to call the Institution Servers
5. Click `Chnage Env` to change the Environment 

### Display User Info

The Institution Servers (in our Demo App set to Ready ODIN), internally calls the Ready Public API using the access token (for authenticating the API), and return the
Public User info which is then displayed on the Demo Webapp.

#### Snippet of ODIN API Implementation

```python
class PublicUserInfo(Endpoint):

    def get(self, request):
        cleaned_data = request.form.cleaned_data
        user_data = PublicAPI.get_public_user(access_token=cleaned_data['access_token'], user_token=cleaned_data['user_token'])
        if user_data:
            return JsonResponse(user_data, status=200, safe=False)
        return JsonResponse({}, status=400)
```


### Errors

- `user_token` is not set in cookies, the Webapp will display error
- `user_token` is expired Ready Public API throws an error
- Institution Server API or the Ready Public API throws an error/exception


### References

- [Ready Public API](https://readyeducationpublicapi.docs.apiary.io/#reference/0/user/returns-students'-information)
- [Authenticated WebApp Tile Component Architecture](https://support.readyeducation.com/hc/en-us/articles/360025452194-Authenticated-Web-App-Tile-Component-Architecture)
- [Ready JS Library](https://gitlab.com/ready-education-public/integration-js/-/blob/master/README.md)
- [Authenticated Web App Tile](https://support.readyeducation.com/hc/en-us/articles/360025351694)
