import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {StorageService} from "./storage.service";
import {timeout} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {userFormMetaInterface} from "../app/models/user.form.meta.interface";


@Injectable()
export class ApiService {
  public api_env_list = [
    {
      name: 'US',
      school_id: 13178,
      urlInt: 'https://integration.oohlalamobile.com/api',
      accessToken: 'live_ndza2Jn1EvCcmfs4qYvZYIMgJCX',
    },
    {
      name: 'CAN',
      school_id: 13895,
      urlInt: 'https://can-integration.oohlalamobile.com/api',
      accessToken: 'live_s7qdo3AnIfON4OUquv11Bpay1Ph',
    },
    {
      name: 'US-Staging',
      school_id: 157,
      urlInt: 'https://int-dev.oohlalamobile.com/api',
      accessToken: 'live_3qTmXvrkoKM515ffqFyJjSFiW9y',
    }
  ];

  static odin_public_auth_scheme = 'PUBToke';
  static odin_public_api_key = '2qe3k59KwqKff6IAJ3wzKgoRzMM6ji92';

  // API Routes
  get_public_user_info = '/v1/public/user_info';

  constructor(
    private http: HttpClient,
    public storage: StorageService
  ) {
  }

  getUserInfo(userMetaInfo: userFormMetaInterface): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: ApiService.odin_public_auth_scheme + ' ' + ApiService.odin_public_api_key
      }),
      params: new HttpParams()
        .set('user_token', userMetaInfo.userToken)
        .set('access_token', userMetaInfo.accessToken)
    };
    return this.http.get<any>(userMetaInfo.backendUrl, httpOptions).pipe(
      timeout(5000) //5 seconds
    );
  }
}
