import {Injectable} from '@angular/core';
import {userResponseInterface} from "../app/models/user-response";

@Injectable()
export class StorageService {
  setSessionApiEnv(envName: number): void {
    window.sessionStorage.setItem('apiEnv', envName.toString());
  }

  getSessionApiEnv(): number {
    let env = window.sessionStorage.getItem('apiEnv');
    if (env !== null) {
      return parseInt(env, 10);
    }
    return 10;
  }

  setUserInfo(userInfo: userResponseInterface): void {
    window.sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
  }

  getUserInfo(): any {
    let userInfo = window.sessionStorage.getItem('userInfo');
    if (userInfo !== null) {
      return userInfo
    }
    return null;
  }
}
