import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common';
import {userFormMetaInterface} from "../../models/user.form.meta.interface";
import {userResponseInterface} from "../../models/user-response";
import {StorageService} from "../../../services/storage.service";

@Component({
  selector: 'app-user-info-display',
  templateUrl: './user-info-display.component.html',
  styleUrls: ['./user-info-display.component.css']
})
export class UserInfoDisplayComponent implements OnInit {
  userResponse: userResponseInterface;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    public storage: StorageService
  ) {
  }

  ngOnInit(): void {
    this.userResponse = JSON.parse(this.storage.getUserInfo());
  }

  goBack(): void {
    this.location.back();
  }

}
