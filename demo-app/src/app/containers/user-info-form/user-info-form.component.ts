import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {userFormMetaInterface} from "../../models/user.form.meta.interface";
import {ApiService} from "../../../services/api.services";
import {userResponseInterface} from "../../models/user-response";
import {StorageService} from "../../../services/storage.service";

declare var ReadyIntegration: any;

@Component({
  selector: 'app-user-public-info',
  templateUrl: './user-info-form.component.html',
  styleUrls: ['./user-info-form.component.css']
})
export class UserInfoFormComponent implements OnInit {
  // fetched from browser cookies using Ready JS library
  // https://gitlab.com/ready-education-public/integration-js/-/blob/master/README.md
  userToken = "";
  // serviceUrl dummy's the Institution Servers. In Demo App it's configured for Ready's ODIN API service
  serviceUrl = "";
  // accessToken for Test School. Generate a new one for your school via Campus Cloud.
  // https://support.readyeducation.com/hc/en-us/articles/360035456133-Ready-Application-Programming-Interface-API-Overview
  accessToken = "";

  userMetaInfo: userFormMetaInterface;
  userResponse: userResponseInterface;
  errorObj: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    public storage: StorageService,
  ) {
  }

  ngOnInit(): void {
    let currentApiEnvIndex = this.storage.getSessionApiEnv();
    this.serviceUrl = this.apiService.api_env_list[currentApiEnvIndex].urlInt + this.apiService.get_public_user_info;
    this.accessToken = this.apiService.api_env_list[currentApiEnvIndex].accessToken;
    this.userMetaInfo = {
      userToken: ReadyIntegration.getUserToken(),
      accessToken: this.accessToken,
      backendUrl: this.serviceUrl
    }
  }

  envChange() {
    return this.router.navigateByUrl('/env');
  }

  // @ts-ignore
  handleSubmit(data: any) {
    let userMetaInfo = <userFormMetaInterface>{
      userToken: data.userToken,
      accessToken: data.accessToken,
      backendUrl: data.serviceUrl
    }
    if (!userMetaInfo.userToken || userMetaInfo.userToken.length == 0) {
      console.log("verify that cookie is set")
      return this.router.navigate([this.router.url, 'user-info']);
    } else {
      //make API call
      this.apiService.getUserInfo(this.userMetaInfo)
        .subscribe(data => {
            this.userResponse = {
              email: data.email,
              student_id: data.student_id,
              sis_id: data.sis_id,
              lms_id: data.lms_id,
              roles: data.roles,
            }
            this.storage.setUserInfo(this.userResponse);
            return this.router.navigate([this.router.url, 'display']);
          },
          error => {
            this.errorObj = JSON.stringify(error);
          });
    }
  }

}
