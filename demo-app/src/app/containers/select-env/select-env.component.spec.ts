import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectEnvComponent } from './select-env.component';

describe('SelectEnvComponent', () => {
  let component: SelectEnvComponent;
  let fixture: ComponentFixture<SelectEnvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectEnvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectEnvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
