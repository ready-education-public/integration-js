import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {StorageService} from "../../../services/storage.service";
import {ApiService} from "../../../services/api.services";


@Component({
  selector: 'app-select-env',
  templateUrl: './select-env.component.html',
  styleUrls: ['./select-env.component.css']
})
export class SelectEnvComponent implements OnInit {
  displayEnv = false;
  selectedApiEnv: number;

  constructor(
    private router: Router,
    public storage: StorageService,
    public apiService: ApiService
  ) {
  }

  changeEnv() {
    this.storage.setSessionApiEnv(this.selectedApiEnv);
    this.displayEnv = false;
    return this.router.navigateByUrl('/' + this.storage.getSessionApiEnv() + '/user-info');
  }

  ngOnInit() {
    this.displayEnv = true;
  }

}
