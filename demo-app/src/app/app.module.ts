import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserInfoFormComponent } from './containers/user-info-form/user-info-form.component';
import {FormsModule} from "@angular/forms";
import { UserInfoDisplayComponent } from './containers/user-info-display/user-info-display.component';
import { ApiService } from "../services/api.services";
import { StorageService } from "../services/storage.service";
import { SelectEnvComponent } from './containers/select-env/select-env.component';

@NgModule({
  declarations: [
    AppComponent,
    UserInfoFormComponent,
    UserInfoDisplayComponent,
    SelectEnvComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    ApiService,
    StorageService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
