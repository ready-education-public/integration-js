import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserInfoFormComponent} from "./containers/user-info-form/user-info-form.component";
import {UserInfoDisplayComponent} from "./containers/user-info-display/user-info-display.component";
import {SelectEnvComponent} from "./containers/select-env/select-env.component";

const routes: Routes = [
  {path: '', redirectTo: 'env', pathMatch: 'full'},
  {path: 'env', component: SelectEnvComponent},
  {path: ':api_env_index/user-info', component: UserInfoFormComponent},
  {path: ':api_env_index/user-info/display', component: UserInfoDisplayComponent},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
