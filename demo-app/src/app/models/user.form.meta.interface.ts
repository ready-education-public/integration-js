export interface userFormMetaInterface {
  userToken: string;
  accessToken: string;
  backendUrl: string;
}
