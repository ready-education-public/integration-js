export interface userResponseInterface {
  email: string;
  student_id: string;
  sis_id: string;
  lms_id: string;
  roles: any;
}
