# Ready Education

## Ready Integration JS Library

Use this JavaScript library to connect your custom webapps with the [Ready Public API](https://readyeducationpublicapi.docs.apiary.io)

### Architecture Overview

* `Your Webapp`
  * implements the `Ready Integration JS Library` to get an authenticated **user token**
  * makes calls to `Your API` with the **user token**
* `Your API`
  * receives calls from `Your Webapp`
  * makes calls to the `Ready Public API` with the **user token** and your **public API key**
* `Ready Public API`
  * returns authenticated user info to `Your API`, which then returns the user info to `Your Webapp`

### How to Use the Ready Integration JS Library

1. Include the Ready Integration script
  ```html
  <script src="libs/ready-integration.js"></script>
  ```
2. Get the authenticated user token
  ```javascript
  const userToken = ReadyIntegration.getUserToken();
  ```
3. Make an HTTP request to your API using the user token
  ```javascript
  const url = `${BASE_URL}/${API_PATH}?${PARAM_KEY}=${userToken}`;
  ```